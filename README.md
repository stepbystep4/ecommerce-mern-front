# E-Commerce - Frontend
Ceci est le frontend du projet E-Commerce.

#Technologie utilisée :
- React
- Node.js
- MongoDB
- Express

# Description du projet :
Le projet E-Commerce est une application web qui vise à fournir une plateforme pour la vente en ligne de divers produits. Le frontend est développé en utilisant la bibliothèque JavaScript React, qui offre une expérience utilisateur interactive et réactive.

# Fonctionnalités :
Le frontend du projet E-Commerce comprendra les fonctionnalités suivantes :

Affichage des produits disponibles à l'achat.
Recherche de produits par catégorie, nom ou mot-clé.
Ajout de produits au panier.
Gestion du panier d'achat avec possibilité de modifier les quantités ou de retirer des articles.
Passage d'une commande et paiement en ligne (intégration à prévoir).
Suivi des commandes en cours.
Authentification des utilisateurs (connexion, inscription, gestion du profil).
Interface d'administration pour gérer les produits, les commandes et les utilisateurs (réservée aux administrateurs).


# Prérequis :
Node.js et npm doivent être installés sur votre machine pour exécuter l'application en mode développement.
## Installation et exécution :
- Clonez ce dépôt sur votre machine :
```bash
git clone https://github.com/votre_utilisateur/E-Commerce.git
```

- Accédez au répertoire du frontend :

```bash
cd E-Commerce/frontend
```
- Installez les dépendances :
```bash
npm install
```
- Lancez l'application en mode développement :
```bash
npm start
```
L'application frontend sera accessible à l'adresse http://localhost:3000 dans votre navigateur.

# Contributions :
Les contributions à ce projet sont les bienvenues. Si vous souhaitez apporter des améliorations, des corrections de bugs ou de nouvelles fonctionnalités, veuillez créer une pull request afin que nous puissions examiner vos modifications.

# Auteurs :
Ce projet est développé par **Rojo Ny Aina Rakoto**.

# Licence :
Ce projet est sous licence MIT. Veuillez consulter le fichier LICENSE pour plus d'informations.